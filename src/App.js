import React, { Component } from "react";
import axios from "axios";
import { Row, Col, Icon, Button, message } from "antd";
import Slider from "react-slick";
import logo from "./resources/images/Logo_Revolut.png";
import Wallet from "./components/Wallet";
import "./App.scss";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wallets: [
        {
          name: "usd",
          displayName: "USD",
          iconType: "dollar",
          htmlCode: "&#36;",
          balance: 20.8999
        },
        {
          name: "eur",
          displayName: "EUR",
          iconType: "euro",
          htmlCode: "&#128;",
          balance: 9.56
        },
        {
          name: "gbp",
          displayName: "GBP",
          iconType: "pound",
          htmlCode: "&#8356;",
          balance: 10.05
        },
        {
          name: "inr",
          displayName: "INR",
          iconType: "inr",
          htmlCode: "&#8356;",
          balance: 20000
        }
      ],
      fromCur: "",
      toCur: "",
      fromCurType: "",
      toCurType: "",
      exchangeRatesObj: {},
      base: "",
      activeSlideFrom: 0,
      activeSlideTo: 1
    };
  }

  currentExchangeRate = () => {
    const {
      wallets,
      exchangeRatesObj,
      activeSlideFrom,
      activeSlideTo
    } = this.state;
    const baseToFromExchangeRate =
      exchangeRatesObj[wallets[activeSlideFrom].displayName];
    const baseToToExchangeRate =
      exchangeRatesObj[wallets[activeSlideTo].displayName];

    const exchangeRate = parseFloat(
      (baseToToExchangeRate / baseToFromExchangeRate).toFixed(4)
    );
    const exchangeRateReverse = parseFloat(
      (baseToFromExchangeRate / baseToToExchangeRate).toFixed(4)
    );
    return { exchangeRate, exchangeRateReverse };
  };

  convert = () => {
    const { fromCur } = this.state;
    const exchangeRates = this.currentExchangeRate();
    const toCurValue = fromCur * exchangeRates.exchangeRate;
    return parseFloat(toCurValue.toFixed(4));
  };

  exchange = () => {
    const {
      wallets,
      activeSlideFrom,
      activeSlideTo,
      fromCur,
      fromCurType,
      toCurType
    } = this.state;

    const toCur = this.convert();

    if (fromCurType === toCurType) return;

    const fromWalletBalance = wallets[activeSlideFrom].balance - fromCur;
    const toWalletBalance = wallets[activeSlideTo].balance + toCur;

    wallets[activeSlideFrom].balance = parseFloat(fromWalletBalance.toFixed(4));
    wallets[activeSlideTo].balance = parseFloat(toWalletBalance.toFixed(4));

    this.setState({
      wallets: wallets,
      fromCur: "",
      toCur: ""
    });
  };

  inputRegex = /^[0-9]{0,6}(\.[0-9]{1,2})?$/;

  handleChange = e => {
    const { wallets, activeSlideFrom } = this.state;
    const { name, value } = e.target;
    if (value < wallets[activeSlideFrom].balance) {
      const validation = this.inputRegex.test(value);
      if (validation) {
        this.setState({ [name]: value }, () => {
          const toWalletValue = this.convert();
          this.setState({ toCur: toWalletValue });
        });
      } else {
        message.error("Please enter amount upto two decimal places only.");
      }
    } else {
      message.error("Amount Exceeded");
      return;
    }
  };

  getCurrencyRates = () => {
    axios
      .get(
        `https://openexchangerates.org/api/latest.json?app_id=8bf7cb8178ca4936a511e09627446a17`
      )
      .then(res => {
        if (res.status === 200) {
          const data = res.data;
          const rates = data.rates;
          const base = data.base;
          this.setState({ base: base, exchangeRatesObj: rates });
        }
      });
  };

  componentWillMount() {
    const { wallets, activeSlideFrom, activeSlideTo } = this.state;
    this.setState({
      fromCurType: wallets[activeSlideFrom].name,
      toCurType: wallets[activeSlideTo].name
    });
  }

  componentDidMount() {
    this.getCurrencyRates();
    this.interval = setInterval(() => {
      this.getCurrencyRates();
    }, 10000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { activeSlideFrom, activeSlideTo, wallets } = this.state;
    const exchangeRates = this.currentExchangeRate();
    const fromWalletSettings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      initialSlide: activeSlideFrom,
      afterChange: current => {
        this.setState({ activeSlideFrom: current });
        this.setState({ fromCurType: wallets[current].name }, () => {
          const toWalletValue = this.convert();
          this.setState({ toCur: toWalletValue });
        });
      }
    };

    const toWalletSettings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      slickGoTo: 1,
      arrows: false,
      initialSlide: activeSlideTo,
      afterChange: current => {
        this.setState({ activeSlideTo: current });
        this.setState({ toCurType: this.state.wallets[current].name }, () => {
          const toWalletValue = this.convert();
          this.setState({ toCur: toWalletValue });
        });
      }
    };

    const fromWallets = this.state.wallets.map(wallet => (
      <Wallet
        key={wallet.name}
        {...wallet}
        onChange={this.handleChange}
        inputName="fromCur"
        inputValue={this.state.fromCur}
        background="#639fca"
        exchangeRate={exchangeRates.exchangeRate}
        toCurrencyIcon={this.state.wallets[this.state.activeSlideTo].iconType}
        pattern={this.inputRegex}
      />
    ));

    const toWallets = this.state.wallets.map(wallet => (
      <Wallet
        key={wallet.name}
        {...wallet}
        inputName="toCur"
        inputValue={this.state.toCur}
        background="#0775c2"
        exchangeRateReverse={exchangeRates.exchangeRateReverse}
        toCurrencyIcon={this.state.wallets[this.state.activeSlideFrom].iconType}
      />
    ));

    return (
      <div>
        <Row>
          <Col
            span={24}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: 60,
              borderBottom: "1px dashed #999",
              marginBottom: 10
            }}
          >
            <img className="logo" src={logo} alt="Revolut" />
          </Col>
        </Row>
        <Row>
          <Col span={8} offset={8}>
            <Row className="exchange-header">
              <Col span={8} className="left-col"></Col>
              <Col span={8}>
                <Icon
                  type={this.state.wallets[this.state.activeSlideFrom].iconType}
                />{" "}
                1 ={" "}
                <Icon
                  type={this.state.wallets[this.state.activeSlideTo].iconType}
                />{" "}
                {exchangeRates.exchangeRate}
              </Col>
              <Col span={8} className="right-col">
                <Button type="primary" onClick={this.exchange}>
                  <strong>Exchange</strong>
                </Button>
              </Col>
            </Row>
            <div>
              <Slider {...fromWalletSettings}>{fromWallets}</Slider>
            </div>
            <div style={{ marginTop: 20 }}>
              <Slider {...toWalletSettings}>{toWallets}</Slider>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
export default App;
