import React from "react";
import { Icon, Input } from "antd";

const Wallet = ({
  displayName,
  iconType,
  balance,
  onChange,
  inputName,
  inputValue,
  background,
  exchangeRate,
  exchangeRateReverse,
  toCurrencyIcon,
  pattern
}) => {
  return (
    <div className="wallet" style={{ backgroundColor: background }}>
      <div className="input-container">
        <div className="left">
          <h1>{displayName}</h1>
        </div>
        <div className="right">
          <Input
            name={inputName}
            type="number"
            step=".01"
            value={inputValue}
            onChange={onChange}
            pattern={pattern}
          />
        </div>
      </div>
      <div className="balance-rates">
        <h5 className="left">
          You have <Icon type={iconType} /> {balance}
        </h5>
        <h5 className="right">
          <Icon type={iconType} /> 1 = <Icon type={toCurrencyIcon} />{" "}
          {exchangeRate} {exchangeRateReverse}
        </h5>
      </div>
    </div>
  );
};

export default Wallet;
